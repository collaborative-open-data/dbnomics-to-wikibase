import fs from "fs"
import path from "path"

import { validateNonEmptyTrimmedString, validateUrl } from "./core"

export function validateConfig(config) {
  if (config === null || config === undefined) {
    return [config, "Missing config"]
  }
  if (typeof config !== "object") {
    return [config, `Expected an object got "${typeof config}"`]
  }

  config = { ...config }
  const errors = {}
  const remainingKeys = new Set(Object.keys(config))

  {
    const key = "udata"
    if (remainingKeys.delete(key)) {
      const [value, error] = validateUdata(config[key])
      config[key] = value
      if (error !== null) {
        errors[key] = error
      }
    } else {
      errors[key] = "Missing item"
    }
  }

  {
    const key = "wikibase"
    if (remainingKeys.delete(key)) {
      const [value, error] = validateWikibase(config[key])
      config[key] = value
      if (error !== null) {
        errors[key] = error
      }
    } else {
      errors[key] = "Missing item"
    }
  }

  for (let key of remainingKeys) {
    errors[key] = "Unexpected item"
  }
  return [config, Object.keys(errors).length === 0 ? null : errors]
}

function validateUdata(udata) {
  if (udata === null || udata === undefined) {
    return [udata, "Missing value"]
  }
  if (typeof udata !== "object") {
    return [udata, `Expected an object got "${typeof udata}"`]
  }

  udata = { ...udata }
  const errors = {}
  const remainingKeys = new Set(Object.keys(udata))

  {
    const key = "dir"
    if (remainingKeys.delete(key)) {
      let dir = udata[key]
      if (typeof dir !== "string") {
        errors[key] = `Expected a string got "${typeof dir}"`
      } else {
        dir = path.resolve(path.normalize(dir.trim()))
        if (!fs.existsSync(dir)) {
          try {
            fs.mkdirSync(dir)
          } catch (e) {
            errors[key] = `Creation of directory "${dir}" failed: ${e}`
          }
        } else {
          if (!fs.lstatSync(dir).isDirectory()) {
            errors[key] = `Node "${dir}" isn't a directory`
          }
        }
      }
      udata[key] = dir
    } else {
      errors[key] = "Missing item"
    }
  }

  {
    const key = "url"
    if (remainingKeys.delete(key)) {
      const [value, error] = validateUrl(udata[key])
      udata[key] = value
      if (error !== null) {
        errors[key] = error
      }
    } else {
      errors[key] = "Missing item"
    }
  }

  for (let key of remainingKeys) {
    errors[key] = "Unexpected item"
  }
  return [udata, Object.keys(errors).length === 0 ? null : errors]
}

function validateWikibase(wikibase) {
  if (wikibase === null || wikibase === undefined) {
    return [wikibase, "Missing value"]
  }
  if (typeof wikibase !== "object") {
    return [wikibase, `Expected an object got "${typeof wikibase}"`]
  }

  wikibase = { ...wikibase }
  const errors = {}
  const remainingKeys = new Set(Object.keys(wikibase))

  for (let key of ["password", "user"]) {
    if (remainingKeys.delete(key)) {
      const [value, error] = validateNonEmptyTrimmedString(wikibase[key])
      wikibase[key] = value
      if (error !== null) {
        errors[key] = error
      }
    } else {
      errors[key] = "Missing item"
    }
  }

  {
    const key = "url"
    if (remainingKeys.delete(key)) {
      const [value, error] = validateUrl(wikibase[key])
      wikibase[key] = value
      if (error !== null) {
        errors[key] = error
      }
    } else {
      errors[key] = "Missing item"
    }
  }

  for (let key of remainingKeys) {
    errors[key] = "Unexpected item"
  }
  return [wikibase, Object.keys(errors).length === 0 ? null : errors]
}
