// Cf https://stackoverflow.com/questions/1303872/trying-to-validate-url-using-javascript
// But added support for "localhost"
const urlRegExp = /^(?:(?:(?:https?|ftp):)?\/\/)(?:\S+(?::\S*)?@)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,})).?)|localhost(?::\d{2,5})?(?:[/?#]\S*)?$/i

export function validateArray(itemValidator) {
  return function(array) {
    if (array === null || array === undefined) {
      return [array, "Missing value"]
    }
    if (!Array.isArray(array)) {
      return [array, `Expected an array, got "${typeof array}"`]
    }
    const errors = {}
    array = [...array]
    for (let [index, value] of array.entries()) {
      const [validatedValue, error] = itemValidator(value)
      array[index] = validatedValue
      if (error !== null) {
        errors[index] = error
      }
    }
    return [array, Object.keys(errors).length === 0 ? null : errors]
  }
}

export function validateChain(validators) {
  return function(value) {
    let error = null
    for (let validator of validators) {
      [value, error] = validator(value)
      if (error !== null) {
        return [value, error]
      }
    }
    return [value, null]
  }
}

export function validateChoice(options) {
  return function (value) {
    if (!options.includes(value)) {
      return [value, "Unexpected option"]
    }
    return [value, null]
  }
}

export function validateEmptyToNull(validator) {
  return function(value) {
    if (value === null || value === undefined) {
      return [null, null]
    }
    if (Array.isArray(value)) {
      if (value.length === 0) {
        return [null, null]
      }
    } else if (typeof value === "object") {
      if (Object.keys(value).length === 0) {
        return [null, null]
      }
    } else if (typeof value === "string") {
      if (!value.trim()) {
        return [null, null]
      }
    }
    return validator(value)
  }
}

export function validateFunction(func) {
  return function(value) {
    return [func(value), null]
  }
}

export function validateNonEmptyTrimmedString(value) {
  if (value === null || value === undefined) {
    return [value, "Missing value"]
  }
  if (typeof value !== "string") {
    return [value, `Expected a string got "${typeof value}"`]
  }
  value = value.trim()
  if (!value) {
    return [value, "Expected a non empty string"]
  }
  return [value, null]
}

export function validateOptional(defaultValue, validator) {
  return function(value) {
    if (value === null || value === undefined) {
      return [defaultValue, null]
    }
    return validator(value)
  }
}

export function validateTest(test, errorMessage) {
  return function(value) {
    return [
      value,
      test(value)
        ? null
        : !errorMessage
          ? "Test failed"
          : typeof errorMessage === "string"
            ? errorMessage
            : errorMessage(value),
    ]
  }
}

export function validateUrl(input) {
  const [value, error] = validateNonEmptyTrimmedString(input)
  if (error !== null) {
    return [value, error]
  }
  if (!urlRegExp.test(value)) {
    return [value, "Invalid URL"]
  }
  // Wikibase-specific tests
  if (value.length > 500) {
    return [value, "URL too long for Wikibase"]
  }
  if (/[ \[\]]/.test(value)) {
    return [value, "An URL can not contain spaces or square brackets"]
  }
  return [value, null]
}
