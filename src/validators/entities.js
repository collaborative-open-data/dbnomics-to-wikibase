import { cleanUpLine, truncate } from "../helpers"
import { validateArray, validateChain, validateChoice, validateEmptyToNull, validateFunction, validateNonEmptyTrimmedString, validateTest } from "./core"

const dataTypes = [
  "commonsMedia",
  "external-id",
  "geo-shape",
  "globe-coordinate",
  "monolingualtext",
  "quantity",
  "math",
  "string",
  "tabular-data",
  "time",
  "url",
  "wikibase-item",
  "wikibase-property",
]

const languages = [
  "en",
  "fr",
]

export function validateAlias(alias) {
  if (alias === null || alias === undefined) {
    return [alias, "Missing alias"]
  }
  if (typeof alias !== "object") {
    return [alias, `Expected an object got "${typeof alias}"`]
  }

  alias = { ...alias }
  const errors = {}
  const remainingKeys = new Set(Object.keys(alias))

  {
    const key = "language"
    remainingKeys.delete(key)
    const [value, error] = validateChain([
      validateNonEmptyTrimmedString,
      validateChoice(languages),
    ])(alias[key])
    alias[key] = value
    if (error !== null) {
      errors[key] = error
    }
  }

  {
    const key = "value"
    remainingKeys.delete(key)
    const [value, error] = validateEmptyToNull(validateChain([
      validateNonEmptyTrimmedString,
      validateFunction(cleanUpLine),
      validateNonEmptyTrimmedString,
    ]))(alias[key])
    alias[key] = value
    if (error !== null) {
      errors[key] = error
    }
  }

  for (let key of remainingKeys) {
    errors[key] = "Unexpected item"
  }
  if (Object.keys(errors).length !== 0) {
    return [alias, errors]
  }
  if (alias.value === null) {
    return [null, null]
  }
  return [alias, null]
}

export function validateDescription(description) {
  if (description === null || description === undefined) {
    return [description, "Missing description"]
  }
  if (typeof description !== "object") {
    return [description, `Expected an object got "${typeof description}"`]
  }

  description = { ...description }
  const errors = {}
  const remainingKeys = new Set(Object.keys(description))

  {
    const key = "language"
    remainingKeys.delete(key)
    const [value, error] = validateChain([
      validateNonEmptyTrimmedString,
      validateChoice(languages),
    ])(description[key])
    description[key] = value
    if (error !== null) {
      errors[key] = error
    }
  }

  {
    const key = "value"
    remainingKeys.delete(key)
    const [value, error] = validateEmptyToNull(validateChain([
      validateNonEmptyTrimmedString,
      validateFunction(value =>  truncate(cleanUpLine(value), 250)),
      validateNonEmptyTrimmedString,
    ]))(description[key])
    description[key] = value
    if (error !== null) {
      errors[key] = error
    }
  }

  for (let key of remainingKeys) {
    errors[key] = "Unexpected item"
  }
  if (Object.keys(errors).length !== 0) {
    return [description, errors]
  }
  if (description.value === null) {
    return [null, null]
  }
  return [description, null]
}

// Validate an item without claims.
export function validateItemCore(item) {
  if (item === null || item === undefined) {
    return [item, "Missing item"]
  }
  if (typeof item !== "object") {
    return [item, `Expected an object got "${typeof item}"`]
  }

  item = { ...item }
  const errors = {}
  const remainingKeys = new Set(Object.keys(item))

  {
    const key = "aliases"
    remainingKeys.delete(key)
    const [value, error] = validateEmptyToNull(validateChain([
      validateArray(validateAlias),
      validateEmptyToNull(validateFunction(aliases => aliases.filter(alias => alias && alias.value))),
    ]))(item[key])
    if (value === null) {
      delete item[key]
    } else {
      item[key] = value
    }
    if (error !== null) {
      errors[key] = error
    }
  }

  {
    const key = "descriptions"
    remainingKeys.delete(key)
    const [value, error] = validateEmptyToNull(validateChain([
      validateArray(validateDescription),
      validateEmptyToNull(validateFunction(descriptions => descriptions.filter(description => description && description.value))),
    ]))(item[key])
    if (value === null) {
      delete item[key]
    } else {
      item[key] = value
    }
    if (error !== null) {
      errors[key] = error
    }
  }

  {
    const key = "labels"
    remainingKeys.delete(key)
    const [value, error] = validateChain([
      validateArray(validateLabel),
      validateFunction(labels => labels.filter(label => label && label.value)),
      validateTest(labels => labels.length >= 1, "Item requires one at least one label"),
    ])(item[key])
    if (value === null) {
      delete item[key]
    } else {
      item[key] = value
    }
    if (error !== null) {
      errors[key] = error
    }
  }

  {
    const key = "sitelinks"
    remainingKeys.delete(key)
    const [value, error] = validateEmptyToNull(validateChain([
      validateArray(validateSiteLink),
      validateEmptyToNull(validateFunction(siteLinks => siteLinks.filter(siteLink => siteLink && siteLink.title))),
    ]))(item[key])
    if (value === null) {
      delete item[key]
    } else {
      item[key] = value
    }
    if (error !== null) {
      errors[key] = error
    }
  }

  for (let key of remainingKeys) {
    errors[key] = "Unexpected item"
  }
  return [item, Object.keys(errors).length === 0 ? null : errors]
}

export function validateLabel(label) {
  if (label === null || label === undefined) {
    return [label, "Missing label"]
  }
  if (typeof label !== "object") {
    return [label, `Expected an object got "${typeof label}"`]
  }

  label = { ...label }
  const errors = {}
  const remainingKeys = new Set(Object.keys(label))

  {
    const key = "language"
    remainingKeys.delete(key)
    const [value, error] = validateChain([
      validateNonEmptyTrimmedString,
      validateChoice(languages),
    ])(label[key])
    label[key] = value
    if (error !== null) {
      errors[key] = error
    }
  }

  {
    const key = "value"
    remainingKeys.delete(key)
    const [value, error] = validateEmptyToNull(validateChain([
      validateNonEmptyTrimmedString,
      validateFunction(value =>  truncate(cleanUpLine(value), 250)),
      validateNonEmptyTrimmedString,
    ]))(label[key])
    label[key] = value
    if (error !== null) {
      errors[key] = error
    }
  }

  for (let key of remainingKeys) {
    errors[key] = "Unexpected item"
  }
  if (Object.keys(errors).length !== 0) {
    return [label, errors]
  }
  if (label.value === null) {
    return [null, null]
  }
  return [label, null]
}

// Validate a property without claims.
export function validatePropertyCore(property) {
  if (property === null || property === undefined) {
    return [property, "Missing property"]
  }
  if (typeof property !== "object") {
    return [property, `Expected an object got "${typeof property}"`]
  }

  property = { ...property }
  const errors = {}
  const remainingKeys = new Set(Object.keys(property))

  {
    const key = "aliases"
    remainingKeys.delete(key)
    const [value, error] = validateEmptyToNull(validateChain([
      validateArray(validateAlias),
      validateEmptyToNull(validateFunction(aliases => aliases.filter(alias => alias && alias.value))),
    ]))(property[key])
    if (value === null) {
      delete property[key]
    } else {
      property[key] = value
    }
    if (error !== null) {
      errors[key] = error
    }
  }

  {
    const key = "datatype"
    remainingKeys.delete(key)
    const [value, error] = validateChain([
      validateNonEmptyTrimmedString,
      validateChoice(dataTypes),
    ])(property[key])
    property[key] = value
    if (error !== null) {
      errors[key] = error
    }
  }

  {
    const key = "descriptions"
    remainingKeys.delete(key)
    const [value, error] = validateEmptyToNull(validateChain([
      validateArray(validateDescription),
      validateEmptyToNull(validateFunction(descriptions => descriptions.filter(description => description && description.value))),
    ]))(property[key])
    if (value === null) {
      delete property[key]
    } else {
      property[key] = value
    }
    if (error !== null) {
      errors[key] = error
    }
  }

  {
    const key = "labels"
    remainingKeys.delete(key)
    const [value, error] = validateChain([
      validateArray(validateLabel),
      validateFunction(labels => labels.filter(label => label && label.value)),
      validateTest(labels => labels.length >= 1, "Property requires one at least one label"),
    ])(property[key])
    if (value === null) {
      delete property[key]
    } else {
      property[key] = value
    }
    if (error !== null) {
      errors[key] = error
    }
  }

  for (let key of remainingKeys) {
    errors[key] = "Unexpected property"
  }
  return [property, Object.keys(errors).length === 0 ? null : errors]
}

export function validateSiteLink(siteLink) {
  if (siteLink === null || siteLink === undefined) {
    return [siteLink, "Missing siteLink"]
  }
  if (typeof siteLink !== "object") {
    return [siteLink, `Expected an object got "${typeof siteLink}"`]
  }

  siteLink = { ...siteLink }
  const errors = {}
  const remainingKeys = new Set(Object.keys(siteLink))

  {
    const key = "site"
    remainingKeys.delete(key)
    // Caution: A site link site must not contain "_", otherwise search fails => use " " instead.
    const [value, error] = validateChain([
      validateNonEmptyTrimmedString,
      validateFunction(title => title.replace(/_/g, " ")),
      validateNonEmptyTrimmedString,
    ])(siteLink[key])
    siteLink[key] = value
    if (error !== null) {
      errors[key] = error
    }
  }

  {
    const key = "title"
    remainingKeys.delete(key)
    // Caution: A site link title must not contain "_", otherwise search fails => use " " instead.
    const [value, error] = validateEmptyToNull(validateChain([
      validateNonEmptyTrimmedString,
      validateFunction(title => title.replace(/_/g, " ")),
      validateNonEmptyTrimmedString,
    ]))(siteLink[key])
    siteLink[key] = value
    if (error !== null) {
      errors[key] = error
    }
  }

  for (let key of remainingKeys) {
    errors[key] = "Unexpected item"
  }
  if (Object.keys(errors).length !== 0) {
    return [siteLink, errors]
  }
  if (siteLink.title === null) {
    return [null, null]
  }
  return [siteLink, null]
}
