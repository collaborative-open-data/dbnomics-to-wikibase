import assert from "assert"
import commandLineArgs from "command-line-args"
import fs from "fs"
import rpn from "request-promise-native"
import path from "path"
import url from "url"

import pkg from "../../package.json"
import { sleep } from "../helpers"
import { assertValid } from "../validators/assert"
import { validateItemCore, validatePropertyCore } from "../validators/entities"


let csrfToken = null
let nbSeriesPropertyId = null
let officialWebsitePropertyId = null
let publisherPropertyId = null
let request = null


function numericIdFromItemId(itemId) {
  return parseInt(itemId.substring(1))
}


async function login() {
  console.log("Login...")

  let result = await request.post(
    url.resolve(process.env.WIKIBASE_URL, "api.php"),
    {
      form: {
        action: "query",
        format: "json",
        meta: "tokens",
        type: "login",
      },
      json: true,
    },
  )
  assert(result.error === undefined, JSON.stringify(result, null, 2))
  // console.log(JSON.stringify(result, null, 2))
  const loginToken = result.query.tokens.logintoken

  result = await request.post(
    url.resolve(process.env.WIKIBASE_URL, "api.php"),
    {
      form: {
        action: "login",
        format: "json",
        lgname: process.env.MEDIAWIKI_BOT_USERNAME,
        lgpassword: process.env.MEDIAWIKI_BOT_PASSWORD,
        lgtoken: loginToken,
      },
      json: true,
    },
  )
  assert(result.error === undefined, JSON.stringify(result, null, 2))
  // console.log(JSON.stringify(result, null, 2))
}


async function main() {
  const optionDefinitions = [
    { name: 'dataDir', type: String, defaultOption: true },
    { name: 'provider', type: String },
    { name: 'dataset', type: String },
  ]
  const options = commandLineArgs(optionDefinitions)
  assert(options.dataDir, `dataDir argument missing`)

  request = rpn.defaults({
    headers: {
      "User-Agent": `${pkg.name}/${pkg.version} (${pkg.repository.url}; ${pkg.author})`,
    },
    jar: true,
  })

  await login()
  await requestCsrfToken()

  nbSeriesPropertyId = (await upsertPropertyCore({
    datatype: "quantity",
    labels: [
      {
        language: "en",
        value: "number of time series",
      },
      {
        language: "fr",
        value: "nombre de séries temporelles",
      }
    ],
  })).id
  console.log("nbSeries property: ", nbSeriesPropertyId)

  officialWebsitePropertyId = (await upsertPropertyCore({
    datatype: "url",
    descriptions: [
      {
        language: "en",
        value: "URL of the official website of an item (current or former) [if the website changes, add an additional statement with preferred rank. Do not remove the former URL]",
      },
      {
        language: "fr",
        value: "URL du site web officiel de l'élément",
      },
    ],
    labels: [
      {
        language: "en",
        value: "official website",
      },
      {
        language: "fr",
        value: "site officiel",
      },
    ],
  })).id
  console.log("official website property: ", officialWebsitePropertyId)

  publisherPropertyId = (await upsertPropertyCore({
    datatype: "wikibase-item",
    descriptions: [
      {
        language: "en",
        value: "organization or person responsible for publishing books, periodicals, games or software",
      },
      {
        language: "fr",
        value: "organisation qui est responsable pour la publication du sujet",
      },
    ],
    labels: [
      {
        language: "en",
        value: "publisher",
      },
      {
        language: "fr",
        value: "éditeur",
      },
    ],
  })).id
  console.log("publisher property: ", publisherPropertyId)

  const providers = JSON.parse(fs.readFileSync(path.join(options.dataDir, "providers.json")))
    .filter(provider => options.provider ? provider.code === options.provider : true)
  for (const provider of providers) {
    const publisherItemId = await upsertProvider(provider)
    const providerDir = path.join(options.dataDir, provider.code)
    const datasetFileNames = fs.readdirSync(providerDir)
      .filter(datasetFileName => options.dataset ? datasetFileName === `${options.dataset}.json` : true)
    for (const datasetFileName of datasetFileNames) {
      const dataset = JSON.parse(fs.readFileSync(path.join(providerDir, datasetFileName)))
      await upsertDataset(provider.code, publisherItemId, dataset)
    }
  }

}


async function postToApi(body) {
  let result = null
  for (let delay of [1, 2, 4, 8, 16, 32, 64, 128, 256, 512, 1024, 2048, 4096, 8192, 16384, 0]) {
    const form = {
      ...body,
      token: csrfToken,
    }
    result = await request.post(
      url.resolve(process.env.WIKIBASE_URL, "api.php"),
      {
        form,
        json: true,
      },
    )
    if (result.error === undefined) {
      return result
    }
    if (
      result.error.messages
      && result.error.messages.some(message =>
        message.name === "wikibase-validator-label-with-description-conflict"
      )
    ) {
      // Duplicate label: Let the caller handle this problem.
      return result
    }
    if (result.error.code === "badtoken") {
      // await logout()
      await login()
      await requestCsrfToken()
      continue
    }
    if (
      result.error.code === "failed-save"
      && result.error.messages
      && result.error.messages.some(message => message.name === "actionthrottledtext")
    ) {
      console.log("A failed-save error occurred:", JSON.stringify(result, null, 2))
      console.log(`Sleeping ${delay} seconds...`)
      sleep(delay)
      continue
    }
    if (result.error.code === "no-external-page") {
      console.log("A no-external-page error occurred:", JSON.stringify(result, null, 2))
      console.log(`Sleeping ${delay} seconds...`)
      sleep(delay)
      continue
    }
    // Unhandled error. Throw an exception.
    break
  }
  assert(result.error === undefined, JSON.stringify(result, null, 2))
}


async function requestCsrfToken() {
  const result = await request.post(
    url.resolve(process.env.WIKIBASE_URL, "api.php"),
    {
      form: {
        action: "query",
        format: "json",
        meta: "tokens",
      },
      json: true,
    },
  )
  assert(result.error === undefined, JSON.stringify(result, null, 2))
  csrfToken = result.query.tokens.csrftoken
  assert(csrfToken !== undefined, JSON.stringify(result, null, 2))
  return csrfToken
}


async function upsertDataset(providerCode, publisherItemId, dataset) {
  const item = await upsertItemCore({
    aliases: dataset.name
      ? [{ language: "en", value: dataset.code }]
      : null,
    descriptions: dataset.description
      ? [{ language: "en", value: dataset.description }]
      : null,
    labels: [{ language: "en", value: dataset.name ? dataset.name : dataset.code }],
    sitelinks: [{
      site: "db.nomics.world",
      title: `${providerCode}/${dataset.code}`,
    }]
  })
  const claims = item.claims
  assert(claims !== undefined, JSON.stringify(item, null, 2))
  let lastrevid = item.lastrevid
  assert(lastrevid !== undefined, JSON.stringify(item, null, 2))

  console.log(`${providerCode}/${dataset.code}`, item.labels.en.value, item.id)

  {
    if (dataset.nb_series) {
      const nbSeriesQuantity = `+${dataset.nb_series}`
      const existingNbSeriesClaim = (claims[nbSeriesPropertyId] || [])
        .filter(claim =>
          claim.mainsnak.snaktype === "value"
          && claim.mainsnak.datavalue.type === "quantity"
          && claim.mainsnak.datavalue.value.amount === nbSeriesQuantity
        )[0]
      if (existingNbSeriesClaim === undefined) {
        // Create new "nb series" claim.
        const result = await postToApi({
          action: "wbcreateclaim",
          baserevid: lastrevid,
          bot: true,
          entity: item.id,
          format: "json",
          property: nbSeriesPropertyId,
          snaktype: "value",
          // summary: "Adding claim",
          value: JSON.stringify({
            amount: nbSeriesQuantity,
            unit: "1",
          }),
        })
        lastrevid = result.pageinfo.lastrevid
        assert(lastrevid !== undefined, JSON.stringify(result, null, 2))
        const claim = result.claim
        assert(claim !== undefined, JSON.stringify(result, null, 2))
      }
    }
  }

  {
    const existingPublisherClaim = (claims[publisherPropertyId] || [])
      .filter(claim =>
        claim.mainsnak.snaktype === "value"
        && claim.mainsnak.datavalue.value["entity-type"] === "item"
        && claim.mainsnak.datavalue.value.id === publisherItemId
      )[0]
    if (existingPublisherClaim === undefined) {
      // Create new publisher claim.
      const result = await postToApi({
        action: "wbcreateclaim",
        baserevid: lastrevid,
        bot: true,
        entity: item.id,
        format: "json",
        property: publisherPropertyId,
        snaktype: "value",
        // summary: "Adding claim",
        value: JSON.stringify({
          "entity-type": "item",
          "numeric-id": numericIdFromItemId(publisherItemId),
        }),
      })
      lastrevid = result.pageinfo.lastrevid
      assert(lastrevid !== undefined, JSON.stringify(result, null, 2))
      const claim = result.claim
      assert(claim !== undefined, JSON.stringify(result, null, 2))
    }
  }

  return item.id
}


// Update or create an item core (without claims).
async function upsertItemCore(data) {
  data = assertValid(validateItemCore(data))
  return upsertValidEntityCore("item", data)
}


async function upsertPropertyCore(data) {
  data = assertValid(validatePropertyCore(data))
  return upsertValidEntityCore("property", data)
}


async function upsertProvider(provider) {
  const item = await upsertItemCore({
    aliases: [
      { language: "en", value: provider.code },
      { language: "fr", value: provider.code }
    ],
    // descriptions: provider.description
    //   ? [{ language: "fr", value: provider.description }]
    //   : null,
    labels: [{
      language: "en",
      value: provider.name,
    }],
    sitelinks: [{
      site: "db.nomics.world",
      title: provider.code,
    }]
  })
  const claims = item.claims
  assert(claims !== undefined, JSON.stringify(item, null, 2))
  let lastrevid = item.lastrevid
  assert(lastrevid !== undefined, JSON.stringify(item, null, 2))

  console.log(provider.code, item.labels.en.value, item.id)

  {
    assert(provider.website)
    const existingOfficialWebsiteClaim = (claims[officialWebsitePropertyId] || [])
      .filter(claim =>
        claim.mainsnak.snaktype === "value"
        && claim.mainsnak.datavalue.value === provider.website
      )[0]
    if (existingOfficialWebsiteClaim === undefined) {
      // Create new "official website" claim.
      const result = await postToApi({
        action: "wbcreateclaim",
        baserevid: lastrevid,
        bot: true,
        entity: item.id,
        format: "json",
        property: officialWebsitePropertyId,
        snaktype: "value",
        // summary: "Adding claim",
        value: JSON.stringify(provider.website),
      })
      lastrevid = result.pageinfo.lastrevid
      assert(lastrevid !== undefined, JSON.stringify(result, null, 2))
      const claim = result.claim
      assert(claim !== undefined, JSON.stringify(result, null, 2))
    }
  }

  return item.id
}


async function upsertValidEntityCore(entityType, data) {
  const firstLabel = { ...data.labels[0] }

  let entity = null
  let result = null
  if (data.sitelinks === undefined) {
    result = await postToApi({
      action: "wbsearchentities",
      format: "json",
      language: firstLabel.language,
      limit: 1,
      search: firstLabel.value,
      type: entityType,
    })
    if (result.search.length > 0 && data.labels.map(label => label.value).includes(result.search[0].label)) {
      result = await postToApi({
        action: "wbgetentities",
        format: "json",
        ids: result.search[0].id,
      })
      assert(result.entities)
      assert(Object.keys(result.entities).length === 1)
      entity = Object.values(result.entities)[0]
    }
  } else {
    const firstSiteLink = data.sitelinks[0]
    result = await postToApi({
      action: "wbgetentities",
      format: "json",
      sites: firstSiteLink.site,
      titles: firstSiteLink.title,
    })
    assert(result.entities)
    assert(Object.keys(result.entities).length === 1)
    entity = Object.values(result.entities)[0]
  }
  if (entity === null || entity.missing !== undefined) {
    // Wikibase entity is missing. Create it.
    for (let i = 0; ; i++) {
      result = await postToApi({
        action: "wbeditentity",
        data: JSON.stringify(data, null, 2),
        format: "json",
        new: entityType,
      })
      if (
        result.error
        && result.error.messages
        && result.error.messages.some(message => message.name === "wikibase-validator-label-with-description-conflict")
      ) {
        // An entity with the same name already exists.
        data.labels[0].value = `${firstLabel.value} (${i + 2})`
        continue
      }
      break
    }
    entity = result.entity
  } else {
    // Check if existing entity needs to be updated.
    const dataChanges = {}
    if (data.aliases !== undefined) {
      if (data.aliases.some(dataAlias => {
        const entityAliases = entity.aliases[dataAlias.language]
        if (entityAliases === undefined) {
          return true
        }
        return !entityAliases.some(entityAlias => entityAlias.value === dataAlias.value)
      })) {
        // Some aliases in data are not present in entity. Add them all.
        dataChanges.aliases = data.aliases.map(alias => {
          alias.add = ""
          return alias
        })
      }
    }
    if (data.datatype !== undefined) {
      assert.strictEqual(
        data.datatype,
        entity.datatype,
        `Existing property ${JSON.stringify(entity, null, 2)} has datatype "${entity.datatype}" different from "${data.datatype}"`)
    }
    if (data.descriptions !== undefined) {
      if (data.descriptions.some(description => {
        const entityDescription = entity.descriptions[description.language]
        return entityDescription === undefined || description.value !== entityDescription.value
      })) {
        // Some descriptions in data are missing from entity. Add them.
        dataChanges.descriptions = data.descriptions
      }
    }
    if (data.labels.some(label => {
      const entityLabel = entity.labels[label.language]
      return entityLabel === undefined || label.value !== entityLabel.value
    })) {
      // Some labels in data are missing from entity. Add them.
      dataChanges.labels = data.labels
    }
    if (data.sitelinks !== undefined) {
      if (data.sitelinks.some(siteLink => {
        const entitySiteLink = entity.sitelinks[siteLink.site]
        return entitySiteLink === undefined || siteLink.title !== entitySiteLink.title
      })) {
        // Some sitelinks in data are missing from entity. Add them.
        dataChanges.sitelinks = data.sitelinks
      }
    }
    if (Object.keys(dataChanges).length > 0) {
      // Update entity.
      console.log(`Updating entity ${entity.id}:`, JSON.stringify(dataChanges, null, 2))
      for (let i = 0; ; i++) {
        result = await postToApi({
          action: "wbeditentity",
          data: JSON.stringify(dataChanges, null, 2),
          format: "json",
          id: entity.id,
        })
        if (
          result.error
          && result.error.messages
          && result.error.messages.some(message => message.name === "wikibase-validator-label-with-description-conflict")
        ) {
          if (dataChanges.labels !== undefined) {
            // An entity with the same name already exists.
            dataChanges.labels[0] = {...data.labels[0]}
            dataChanges.labels[0].value = `${firstLabel.value} (${i + 2})`
            continue
          } else if (dataChanges.descriptions !== undefined) {
            // An entity with the same label & description already exists.
            dataChanges.labels = [...data.labels]
            dataChanges.labels[0] = { ...data.labels[0] }
            dataChanges.labels[0].value = `${firstLabel.value} (${i + 2})`
            continue
          }
        }
        break
      }
      entity = result.entity
    }
  }
  return entity
}


require('dotenv').config()

main()
  .catch(error => {
    console.log(error.stack || error)
    process.exit(1)
  })
