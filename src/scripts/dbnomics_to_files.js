const assert = require("assert")
const commandLineArgs = require("command-line-args")
const commandLineCommands = require('command-line-commands')
const fs = require("fs")
const fetch = require("node-fetch")
const path = require("path")
const stringify = require('json-stable-stringify')
const url = require("url")

const dataTypes = ["datasets", "providers"]

const API_URL = "https://api.db.nomics.world/v21"
const DATA_DIR = "db.nomics.world"


// Util


const flatMap = (array, cb) => [].concat(...array.map(cb))


async function fetchJson(apiUrl) {
  console.log(`Fetching "${apiUrl}"`)
  const response = await fetch(apiUrl)
  const result = await response.json()
  // console.log(result)
  return result
}


function writeJsonFileSync(path, data) {
  fs.writeFileSync(path, stringify(data, {space: 2}))
  console.log(`File '${path}' written`)
}


// Fetch entities


async function fetchCategoryTree(providerCode) {
  const apiUrl = url.resolve(API_URL, `/providers/${providerCode}`)
  const result = await fetchJson(apiUrl)
  return result.category_tree
}


async function fetchDataset(providerCode, datasetCode) {
  const apiUrl = url.resolve(API_URL, `/${providerCode}/${datasetCode}/series`)
  const result = await fetchJson(apiUrl)
  return result.dataset
}


async function fetchProviders() {
  const apiUrl = url.resolve(API_URL, "/providers")
  const result = await fetchJson(apiUrl)
  return result.providers.docs
}


// Data processing


function extractDatasetsFromCategoryTree(categoryTree) {
  return flatMap(categoryTree, node => {
    return node.children
      ? extractDatasetsFromCategoryTree(node.children)
      : node
  })
}


// Harvesting


async function harvestDatasets(providerCode) {
  const providerDir = path.join(DATA_DIR, providerCode)
  if (!fs.existsSync(providerDir)) {
    fs.mkdirSync(providerDir)
  }

  // This is a hack: the API should export the list of datasets instead of extracting them from category tree.
  const categoryTree = await fetchCategoryTree(providerCode)
  assert(categoryTree, `Provider '${providerCode}' has no category tree`)
  const datasetCodes = extractDatasetsFromCategoryTree(categoryTree)
    .map(dataset => dataset.code)
    .sort((a, b) => a.localeCompare(b))
  for (const datasetCode of datasetCodes) {
    const datasetPath = path.join(providerDir, `${datasetCode}.json`)
    if (fs.existsSync(datasetPath)) {
      console.log(`Skipping dataset "${datasetCode}" because file ${datasetPath} already exists`)
      continue
    }
    const dataset = await fetchDataset(providerCode, datasetCode)
    if (!dataset || !dataset.code) {
      console.error(`Error with dataset ${providerCode}/${datasetCode}: ${dataset}`)
      continue
    }
    writeJsonFileSync(datasetPath, dataset)
  }
}


async function harvestProviders() {
  const providers = await fetchProviders()
  writeJsonFileSync(path.join(DATA_DIR, `providers.json`), providers)
  return providers
}

// Main


async function main() {
  const validCommands = [null, 'providers', 'datasets']
  const { command, argv } = commandLineCommands(validCommands)

  if (!fs.existsSync(DATA_DIR)) {
    fs.mkdirSync(DATA_DIR)
  }

  switch (command) {
    case null:
      const providers = await harvestProviders()
      for (const provider of providers) {
        await harvestDatasets(provider.code)
      }
      break
    case "datasets":
      const optionDefinitions = [
        { name: 'providerCode', type: String, defaultOption: true }
      ]
      const options = commandLineArgs(optionDefinitions, { argv })
      assert(options.providerCode, `Provider code missing`)
      const { providerCode } = options
      await harvestDatasets(providerCode)
      break
    case "providers":
      await harvestProviders()
      break
  }

}

main().catch(error => {
  console.log(error.stack || error)
  process.exit(1)
})
