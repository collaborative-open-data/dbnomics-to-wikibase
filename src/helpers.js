export function cleanUpLine(text) {
  text = text.trim()
  if (text.startsWith('"') && text.endsWith('"')) {
    text = text.substring(1, text.length - 1).trim()
  }
  return text
    .split(/\s+/)
    .join(" ")
}

export function msleep(n) {
  Atomics.wait(new Int32Array(new SharedArrayBuffer(4)), 0, 0, n)
}

export function sleep(n) {
  msleep(n * 1000)
}

export function truncate(text, length) {
  if (text.length > length) {
    text = text.substring(0, length - 1) + '…'
  }
  return text
}