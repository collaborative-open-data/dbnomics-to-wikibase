# DBnomics_to_Wikibase

_Fetch a DBnomics repository and upload its content to a MediaWiki/Wikibase server._

See [Wikidata mapping for datasets](https://www.wikidata.org/wiki/Wikidata:WikiProject_Datasets/Data_Structure/DCAT_-_Wikidata_-_Schema.org_mapping)

## Installation & Configuration

```bash
git clone https://framagit.org/collaborative-open-data/dbnomics-to-wikibase.git
cd dbnomics-to-wikibase/
npm install
```

## Fetch DBnomics repository

```bash
npx babel-node src/scripts/dbnomics_to_files.js
```

## Upload DBnomics JSON files to Wikibase server

To run this script you must have created a MediaWiki bot on [this page](https://data.gouv2.fr/index.php/Special:BotPasswords/). To do this you must have special rights.

If you have a bot, create a `.env` file and complete the username and password, based on this snippet:

```
WIKIBASE_URL="https://data.gouv2.fr/"
MEDIAWIKI_BOT_USERNAME=""
MEDIAWIKI_BOT_PASSWORD=""
```

```bash
npx babel-node src/scripts/dbnomics_files_to_wikibase.js

# Process all datasets of a provider
npx babel-node src/scripts/dbnomics_files_to_wikibase.js db.nomics.world --provider AMECO

# Process one particular dataset
npx babel-node src/scripts/dbnomics_files_to_wikibase.js db.nomics.world --provider AMECO --dataset ADGGI
```
